const csv = require("csv-parser");
const fs = require("fs");
const Nexmo = require("nexmo");

const MP3_URL = "http://161.35.234.122/resources/audio.mp3";
const MIN_SECONDS = 5;
const DELAY_SECONDS = 20000;
const CSV_PATH = "public/resources/data.csv";

const nexmo = new Nexmo({
	apiKey: "825bd3ff",
	apiSecret: "SGUINPzgE9QWLYEV",
	applicationId: "26da26d0-04b6-4d4c-86cb-3d04501e79e3",
	privateKey: "runner/private.key",
});

getCallResult = (result) => {
	return new Promise((resolve, reject) => {
		nexmo.calls.get(result.uuid, (err, res) => {
			if (err) reject(err);
			resolve(res);
		});
	});
};

call = (phone, ncco) => {
	if (!isValidToRun()) {
		return;
	}
	console.log("in");
	nexmo.calls.create(
		{
			to: [{ type: "phone", number: phone }],
			from: { type: "phone", number: phone },
			ncco,
		},
		async (err, result) => {}
	);
};

function sleep(ms) {
	return new Promise((resolve) => setTimeout(resolve, ms));
}

function getStartTime() {
	const store = require("data-store")({
		path: "public/resources/store.json",
	});

	return store.get("startTime");
}
function getEndTime() {
	const store = require("data-store")({
		path: "public/resources/store.json",
	});

	return store.get("endTime");
}
function getStatus() {
	const store = require("data-store")({
		path: "public/resources/store.json",
	});

	return store.get("status");
}

function isValidToRun() {
	var d = new Date();
	var h = d.getHours();
	console.log(getStartTime() + " " + getEndTime());
	return getStatus() && h >= getStartTime() && h <= getEndTime();
}

function sleep(ms) {
	return new Promise((resolve, reject) => {
		setTimeout(resolve, ms);
	});
}

async function main() {
	while (true) {
		const results = [];
		if (!isValidToRun()) continue;

		fs.createReadStream(CSV_PATH)
			.pipe(csv())
			.on("data", (data) => {
				results.push(data);
				console.log(data);
			})
			.on("end", () => {
				const ncco = [
					{
						action: "stream",
						streamUrl: [MP3_URL],
					},
				];
				for (var i = 0; i < results.length; i++) {
					call(results[i]["phone"], ncco);
				}
			});
		await sleep(DELAY_SECONDS);
	}
}
main();
