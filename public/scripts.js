$(document).ready(function () {
	$("#phone-save").on("click", function () {
		let data = new FormData();
		let files = $("#phone").prop("files");

		if (files.length > 0) {
			data.append("data", files[0]);

			$.ajax({
				url: "/data",
				type: "post",
				data,
				processData: false,
				contentType: false,
				success: function (response) {
					console.log(response);
					alert("Cập nhật danh sách SĐT thành công !");
				},
				error: function (response) {
					console.log(response);
					alert("Đã có lỗi xảy ra hoặc hệ thống đang hoạt động !");
				},
			});
		} else {
			alert("Vui lòng chọn file !");
		}
	});
	$("#sound-save").on("click", function () {
		const data = new FormData();
		const files = $("#sound").prop("files");

		if (files.length > 0) {
			data.append("data", files[0]);

			$.ajax({
				url: "/sound",
				type: "post",
				data,
				processData: false,
				contentType: false,

				success: function (response) {
					alert("Cập nhật âm thanh thành công !");
				},
				error: function (response) {
					console.log(response);
					alert("Đã có lỗi xảy ra hoặc hệ thống đang hoạt động !");
				},
			});
		} else {
			alert("Vui lòng chọn file !");
		}
	});
	$("#time-save").on("click", function () {
		const data = {
			startTime: $("#start-time").val(),
			endTime: $("#end-time").val(),
		};

		if (
			!data.startTime ||
			!data.endTime ||
			data.endTime > 24 ||
			data.startTime > 24 ||
			data.startTime >= data.endTime
		) {
			alert("Dữ liệu sai ! Vui lòng kiểm tra và nhập lại !");
			return;
		}

		$.ajax({
			url: "/set-time",
			type: "post",
			data,
			success: function (response) {
				alert("Cập nhật thời gian thành công !");
			},
			error: function (response) {
				console.log(response);
				alert("Đã có lỗi xảy ra hoặc hệ thống đang hoạt động !");
			},
		});
	});
	$("#system-switch").on("click", function () {
		localStorage.setItem("system", !localStorage.getItem("system"));
		$.ajax({
			url: "/toggle-running",
			type: "post",
			success: function (response) {
				console.log(response);
			},
			error: function (response) {
				console.log(response);
				alert("Đã có lỗi xảy ra !");
			},
		});
	});

	const setSwitch = (status) => {
		$("#system-switch").removeClass("active");
		if (status == "true") {
			$("#system-switch").addClass("active");
		}
	};

	$.ajax({
		url: "/get-status",
		type: "get",
		success: function (response) {
			localStorage.setItem("system", response.status);
			setSwitch(localStorage.getItem("system"));
		},
		error: function (response) {
			console.log(response);
			alert("Đã có lỗi xảy ra !");
		},
	});

	$.ajax({
		url: "/get-time",
		type: "get",
		success: function (response) {
			console.log(
				"🚀 ~ file: scripts.js ~ line 120 ~ response",
				response
			);
			$("#start-time").val(response.startTime);
			$("#end-time").val(response.endTime);
		},
		error: function (response) {
			console.log(response);
			alert("Đã có lỗi xảy ra !");
		},
	});
});
