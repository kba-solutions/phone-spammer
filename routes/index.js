var express = require("express");
var router = express.Router();

const store = require("data-store")({ path: "public/resources/store.json" });

/* GET home page. */
router.get("/", function (req, res, next) {
	res.sendFile(path.join(__dirname + "/public/index.html"));
});

function uploadFile(req, res, filename) {
	let sampleFile;
	let uploadPath;
	console.log(req);

	if (!req.files || Object.keys(req.files).length === 0) {
		return res.status(400).send("No files were uploaded.");
	}

	// The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
	sampleFile = req.files.data;
	uploadPath = "public/resources/" + filename;

	// Use the mv() method to place the file somewhere on your server
	sampleFile.mv(uploadPath, function (err) {
		if (err) return res.status(500).send(err);

		res.send("File uploaded!");
	});
}

function getStatus() {
	return store.get("status");
}

router.post("/data", function (req, res) {
	if (getStatus()) return res.status(400).send("Service is running !");
	uploadFile(req, res, "data.csv");
});

router.post("/sound", function (req, res) {
	if (getStatus()) return res.status(400).send("Service is running !");
	uploadFile(req, res, "audio.mp3");
});

router.post("/set-time", function (req, res) {
	if (getStatus()) return res.status(400).send("Service is running !");
	const startTime = req.body.startTime;
	const endTime = req.body.endTime;
	if (!startTime || !endTime) return res.status(400).send("Invalid Data");
	try {
		store.set("startTime", startTime);
		store.set("endTime", endTime);
		return res.status(200).send("Data Stored");
	} catch (e) {
		return res.status(400).send("Failed to set data");
	}
});

router.get("/get-time", function (req, res) {
	const startTime = store.get("startTime");
	const endTime = store.get("endTime");
	if (!startTime || !endTime) return res.status(400).send("No data");
	return res.status(200).json({ startTime, endTime });
});

router.post("/toggle-running", function (req, res) {
	const status = store.get("status");
	console.log(status);
	const newStatus = !status;
	try {
		store.set("status", newStatus);
		return res.status(200).json({ status: newStatus });
	} catch (e) {
		return res.status(400).send("Failed to set data");
	}
});

router.get("/get-status", function (req, res) {
	const status = store.get("status");
	return res.status(200).json({ status });
});

module.exports = router;
